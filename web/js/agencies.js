$(function() {
    var slider = document.getElementById('slider');
    var left_range = $('#left-range');
    var right_range = $('#right-range');
    var modal = [];
    $('.agencies-item').each(function(index, element){
        modal[index] = Number($(this).attr("numberObjects"));
    });

    $.ajax({
        type: "POST",
        url: "/max/objects",
        success: function(data) {
            var max = Number(data['0']['1']);
            var min = Number(data['0']['2']);
            var value = max-min;

            noUiSlider.create(slider, {
                start: [min, max],
                connect: true,
                step: value*0.01,
                range: {
                    'min': min,
                    'max': max
                }

            });

            slider.noUiSlider.on('update', function( values, handle ) {
                left_range.html(Math.round(values[0]));
                right_range.html(Math.round(values[1]));
            });


            slider.noUiSlider.on('change', function( values, handle ) {
                for(var i = 0; i < modal.length; i++) {

                    $neededDiv = $("div[numberObjects=" + modal[i] + "]");

                    if ( modal[i] < values[0] || modal[i] > values[1]) {
                        $neededDiv.addClass('none');
                    } else if ( modal[i] > values[0] ||
                        modal[i] < values[1] &&
                        $neededDiv.hasClass('none' )
                    ) {
                        $neededDiv.removeClass('none')
                    }
                }

            });
        }
    });

});

$(function() {
    $('.agencies-link').click(function (event) {
        var id = $(this).attr('id');
        $.ajax({
            type: "POST",
            url: "/main/agency/search/"+id,
            success: function(data) {
                var agencies = eval( '('+data+')' );
                var date = new Date(agencies['createdAt']['timestamp']*1000);

                $('#name').html(agencies['name']);
                $('#address').html(agencies['address']);
                $('#phone').html('+375 '+agencies['phone']);
                $('#secondPhone').html('+375 '+agencies['secondPhone']);
                $('#thirdPhone').html('+375 '+agencies['thirdPhone']);
                $('#license').html('Лицензия: '+agencies['license']);
                $('#logo').attr("src", "images/agencies/"+agencies['logo']);
                $('#site').attr("href", "http://"+agencies["site"]).html(agencies['site']);
                $('#email').html(agencies['email']);
                $('#date').html(date.getDate()+'-'+date.getMonth()+1+'-'+date.getFullYear());


            }
        });
    });
});
