$(function() {
    var slider = document.getElementById('sliderAdvert');
    var left_range = $('#left-range');
    var right_range = $('#right-range');
    var priceArray = [];
    $(".advert-item").each(function(index, element){
        priceArray[index] = Number($(this).attr("price"));
    });

    $.ajax({
        type: "POST",
        url: "/max/min/price",
        success: function(data) {
            var max = Number(data['0']['1']);
            var min = Number(data['0']['2']);
            var value = max-min;

            noUiSlider.create(slider, {
                start: [min, max],
                connect: true,
                step: value*0.01,
                range: {
                    'min': min,
                    'max': max
                }

            });

            slider.noUiSlider.on('update', function( values, handle ) {
                left_range.html(Math.round(values[0]));
                right_range.html(Math.round(values[1]));
            });

            slider.noUiSlider.on('change', function( values, handle ) {

                for(var i = 0; i < priceArray.length; i++) {

                    $neededDiv = $("div[price=" + priceArray[i] + "]");

                    if ( ( priceArray[i] < values[0] ||
                        priceArray[i] > values[1] ) &&
                        !$neededDiv.hasClass('none')
                    ) {
                        $neededDiv.addClass('none');
                        console.log($neededDiv);
                    } else if ( (priceArray[i] > values[0] ||
                        priceArray[i] < values[1]) &&
                        $neededDiv.hasClass('none')
                    ) {
                        $neededDiv.removeClass('none');
                    }
                }

            });
        }
    });

});