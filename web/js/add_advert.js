var fileInput = $('#advert_image_files');

var imgList = $('#img-list');

var dropBox = $('#filedrag');

var $form = $('.box');

$(function () {
    $('select').addClass('form-control');

    $('#myTab a:first').tab('show');
});

$(function () {
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        var activeTab = $(e.target);

        if( activeTab.hasClass('disabled') ) {
           e.relatedTarget.tab('show');
        }


    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var activeTab = $(e.target).text();
        $(e.relatedTarget).parent().addClass('success');
        if( activeTab == "Личные данные" ) {
            $('#next').css('display', 'none');
        } else {
            $('#next').css('display', 'inline-block');
        }
    });

});

$(function() {

    function displayFiles(files) {
        var imageType = /image.*/;

        $.each(files, function(i, file) {

            if (!file.type.match(imageType)) {
                return true;
            }

            var div = $('<div class="text-center col-lg-2 col-md-3 col-sm-4 col-xs-6 ">').appendTo(imgList);
            $('<div class="text-center col-lg-12 col-sm-12 col-md-12 col-xs-12 img-header"> <br>').appendTo(div);
            var img = $('<img>').appendTo(div);
            $('<div class="text-center col-lg-12 col-sm-12 col-md-12 col-xs-12 info-div">').appendTo(div)
                .append('<p>' + file.name + '</p>')
                .append('<p> (' + (file.size / 1048576).toFixed(2) + 'MB) </p>');
            $('<div class="text-center col-lg-12 col-sm-12 col-md-12 col-xs-12 delete"> ').appendTo(div)
                .append('<i class="glyphicon glyphicon-trash"></i> Удалить');
            imgList.get(0).file = file;

            // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
            // инфу обо всех файлах
            var reader = new FileReader();
            reader.onload = (function(aImg) {
                return function(e) {
                    aImg.attr('src', e.target.result);
                    aImg.attr('class', 'upload-img');
                    aImg.attr('name', file.name);
                };
            })(img);

            reader.readAsDataURL(file);
        });
    }

    fileInput.on('change', function() {
        displayFiles(this.files);
        $( document ).on( "click", ".delete", function() {
            unset(this.files[i]);
        });
    });

    $form.on('drag dragstart dragend dragover dragenter dragleave drop', function(e) {
        e.preventDefault();
        e.stopPropagation();
    })
        .on('dragover dragenter', function() {
            $form.addClass('is-dragover');
        })
        .on('drop', function(e) {
            var dt = e.originalEvent.dataTransfer;
            displayFiles(dt.files);
            return false
        });

    $('.caption-box').on('dragleave dragend', function() {
        $form.removeClass('is-dragover');
    });

    $( document ).on( "click", ".upload-img", function() {
        var active = imgList.find('div.active');
        active.removeClass('active');
        active.find('div.img-header').html("<br>");
        $(this).parent().addClass('active');
        $(this).prev().text('Заставка');
        $('#advert_image_headpiece').attr('value', $(this).attr('name'));
    });

    if( window.File && window.FileList && window.FileReader )
    {
        dropBox.css('display', 'block');
    }

});


$(function () {
    $('#next').on('click', function () {

        var formValid = true;
        $( $('#myTab li.active a').attr('href') ).find('input').each(function() {

            if ( !this.checkValidity() ) {
                formValid = false;
            }
        });

        if (formValid) {
            $('#myTab li.active').next().children().removeClass('disabled').tab('show');
        }
    });
});