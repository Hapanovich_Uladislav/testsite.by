<?php

namespace AppBundle\Form;

use AppBundle\Entity\Advert;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

class AdvertType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => ['placeholder' => 'form.username']])
            ->add('description', TextareaType::class, ['label_attr' => ['class' => 'none'], 'attr' => ['rows' => '7']])
            ->add('price', NumberType::class)
            ->add('address', TextType::class, ['label' => 'Адрес'])
            ->add('floor', NumberType::class, ['label' => 'Этаж'])
            ->add('floorInHouse', NumberType::class, ['label' => 'Этажей в доме'])
            ->add('totalArea', NumberType::class, ['label' => 'Общая площадь'])
            ->add('livingSpace', NumberType::class, ['label' => 'Жилая площадь'])
            ->add('kitchenArea', NumberType::class, ['label' => 'Площадь кухни'])
            ->add('ceilingHeight', NumberType::class, ['label' => 'Высота потолков'])
            ->add('numberOfRooms', NumberType::class, ['label' => 'Количество комнат'])
            ->add('yearOfConstruction', NumberType::class, ['label' => 'form.yearOfConstruction'])
            ->add('phone', NumberType::class)
            ->add('timeToCallStart', TimeType::class, ['with_minutes' => false])
            ->add('timeToCallEnd', TimeType::class, ['with_minutes' => false])
            ->add('image', ImageType::class, ['label_attr' => ['class' => 'none']])
            ->add('save', SubmitType::class, ['label' => 'Отправить', 'attr' => ['class' => 'btn btn-success btn-lg']]);
    }

}
