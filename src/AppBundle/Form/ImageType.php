<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class ImageType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('files', FileType::class, [
                'label_attr' => ['class' => 'none'],
                'multiple' => true,
                'data_class' => null,
                'attr' => [
                    'class' => 'box__file',
                    'accept' => 'image/*',
                    'data-multiple-caption' => '{count} files selected'
                ]
            ])
            ->add('headpiece', HiddenType::class);
    }

}