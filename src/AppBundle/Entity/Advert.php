<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="advert")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdvertRepository")
 */
class Advert
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * @ORM\Column(type="string", length=40)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $address;

    /**
     * @ORM\Column(type="smallint")
     */
    private $floor;

    /**
     * @ORM\Column(type="smallint")
     */
    private $floorInHouse;

    /**
     * @ORM\Column(type="smallint")
     */
    private $totalArea;

    /**
     * @ORM\Column(type="smallint")
     */
    private $livingSpace;

    /**
     * @ORM\Column(type="smallint")
     */
    private $kitchenArea;

    /**
     * @ORM\Column(type="smallint")
     */
    private $ceilingHeight;

    /**
     * @ORM\Column(type="smallint")
     */
    private $numberOfRooms;

    /**
     * @ORM\Column(type="smallint")
     */
    private $yearOfConstruction;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $phone;

    /**
     * @ORM\Column(type="time", length=11)
     */
    private $timeToCallStart;

    /**
     * @ORM\Column(type="time", length=11)
     */
    private $timeToCallEnd;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAdvert;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->image = new ArrayCollection();
        $this->dateAdvert = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Advert
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Advert
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Advert
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Advert
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set floor
     *
     * @param integer $floor
     *
     * @return Advert
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return integer
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set floorInHouse
     *
     * @param integer $floorInHouse
     *
     * @return Advert
     */
    public function setFloorInHouse($floorInHouse)
    {
        $this->floorInHouse = $floorInHouse;

        return $this;
    }

    /**
     * Get floorInHouse
     *
     * @return integer
     */
    public function getFloorInHouse()
    {
        return $this->floorInHouse;
    }

    /**
     * Set totalArea
     *
     * @param integer $totalArea
     *
     * @return Advert
     */
    public function setTotalArea($totalArea)
    {
        $this->totalArea = $totalArea;

        return $this;
    }

    /**
     * Get totalArea
     *
     * @return integer
     */
    public function getTotalArea()
    {
        return $this->totalArea;
    }

    /**
     * Set livingSpace
     *
     * @param integer $livingSpace
     *
     * @return Advert
     */
    public function setLivingSpace($livingSpace)
    {
        $this->livingSpace = $livingSpace;

        return $this;
    }

    /**
     * Get livingSpace
     *
     * @return integer
     */
    public function getLivingSpace()
    {
        return $this->livingSpace;
    }

    /**
     * Set kitchenArea
     *
     * @param integer $kitchenArea
     *
     * @return Advert
     */
    public function setKitchenArea($kitchenArea)
    {
        $this->kitchenArea = $kitchenArea;

        return $this;
    }

    /**
     * Get kitchenArea
     *
     * @return integer
     */
    public function getKitchenArea()
    {
        return $this->kitchenArea;
    }

    /**
     * Set ceilingHeight
     *
     * @param integer $ceilingHeight
     *
     * @return Advert
     */
    public function setCeilingHeight($ceilingHeight)
    {
        $this->ceilingHeight = $ceilingHeight;

        return $this;
    }

    /**
     * Get ceilingHeight
     *
     * @return integer
     */
    public function getCeilingHeight()
    {
        return $this->ceilingHeight;
    }

    /**
     * Set numberOfRooms
     *
     * @param integer $numberOfRooms
     *
     * @return Advert
     */
    public function setNumberOfRooms($numberOfRooms)
    {
        $this->numberOfRooms = $numberOfRooms;

        return $this;
    }

    /**
     * Get numberOfRooms
     *
     * @return integer
     */
    public function getNumberOfRooms()
    {
        return $this->numberOfRooms;
    }

    /**
     * Set yearOfConstruction
     *
     * @param integer $yearOfConstruction
     *
     * @return Advert
     */
    public function setYearOfConstruction($yearOfConstruction)
    {
        $this->yearOfConstruction = $yearOfConstruction;

        return $this;
    }

    /**
     * Get yearOfConstruction
     *
     * @return integer
     */
    public function getYearOfConstruction()
    {
        return $this->yearOfConstruction;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Advert
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set timeToCallStart
     *
     * @param \DateTime $timeToCallStart
     *
     * @return Advert
     */
    public function setTimeToCallStart($timeToCallStart)
    {
        $this->timeToCallStart = $timeToCallStart;

        return $this;
    }

    /**
     * Get timeToCallStart
     *
     * @return \DateTime
     */
    public function getTimeToCallStart()
    {
        return $this->timeToCallStart;
    }

    /**
     * Set timeToCallEnd
     *
     * @param \DateTime $timeToCallEnd
     *
     * @return Advert
     */
    public function setTimeToCallEnd($timeToCallEnd)
    {
        $this->timeToCallEnd = $timeToCallEnd;

        return $this;
    }

    /**
     * Get timeToCallEnd
     *
     * @return \DateTime
     */
    public function getTimeToCallEnd()
    {
        return $this->timeToCallEnd;
    }

    /**
     * Set dateAdvert
     *
     * @param \DateTime $dateAdvert
     *
     * @return Advert
     */
    public function setDateAdvert($dateAdvert)
    {
        $this->dateAdvert = $dateAdvert;

        return $this;
    }

    /**
     * Get dateAdvert
     *
     * @return \DateTime
     */
    public function getDateAdvert()
    {
        return $this->dateAdvert;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Advert
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

}
