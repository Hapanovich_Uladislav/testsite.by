<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as  ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AgenciesRepository")
 * @ORM\Table(name="agencies")
 */
class Agencies
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(name="number_objects", type="smallint")
     */
    private $numberObjects;

    /**
     * @ORM\Column(name="address", type="string", length=100)
     */
    private $address;

    /**
     * @ORM\Column(name="working_hours", type="string", length=50)
     */
    private $workingHours;

    /**
     * @ORM\Column(name="phone", type="string", length=100)
     */
    private $phone;

    /**
     * @ORM\Column(name="second_phone", type="string", length=100, nullable=true)
     */
    private $secondPhone;

    /**
     * @ORM\Column(name="third_phone", type="string", length=100, nullable=true)
     */
    private $thirdPhone;

    /**
     * @ORM\Column(name="email", type="string", length=40)
     */
    private $email;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="date")
     */
    private $createdAt;

    /**
     * @ORM\Column(name="license", type="string", length=100)
     */
    private $license;

    /**
     * @ORM\Column(name="logo", type="string", length=20)
     */
    private $logo;

    /**
     * @ORM\Column(name="site", type="string", length=30)
     */
    private $site;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Agencies
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set numberObjects
     *
     * @param integer $numberObjects
     *
     * @return Agencies
     */
    public function setNumberObjects($numberObjects)
    {
        $this->numberObjects = $numberObjects;

        return $this;
    }

    /**
     * Get numberObjects
     *
     * @return integer
     */
    public function getNumberObjects()
    {
        return $this->numberObjects;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Agencies
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set workingHours
     *
     * @param string $workingHours
     *
     * @return Agencies
     */
    public function setWorkingHours($workingHours)
    {
        $this->workingHours = $workingHours;

        return $this;
    }

    /**
     * Get workingHours
     *
     * @return string
     */
    public function getWorkingHours()
    {
        return $this->workingHours;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Agencies
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set secondPhone
     *
     * @param string $secondPhone
     *
     * @return Agencies
     */
    public function setSecondPhone($secondPhone)
    {
        $this->secondPhone = $secondPhone;

        return $this;
    }

    /**
     * Get secondPhone
     *
     * @return string
     */
    public function getSecondPhone()
    {
        return $this->secondPhone;
    }

    /**
     * Set thirdPhone
     *
     * @param string $thirdPhone
     *
     * @return Agencies
     */
    public function setThirdPhone($thirdPhone)
    {
        $this->thirdPhone = $thirdPhone;

        return $this;
    }

    /**
     * Get thirdPhone
     *
     * @return string
     */
    public function getThirdPhone()
    {
        return $this->thirdPhone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Agencies
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Agencies
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set license
     *
     * @param string $license
     *
     * @return Agencies
     */
    public function setLicense($license)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license
     *
     * @return string
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Agencies
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return Agencies
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }
}
