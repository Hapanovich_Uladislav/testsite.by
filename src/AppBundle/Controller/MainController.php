<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Agencies;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $agenciesRepository = $this->getDoctrine()->getRepository(Agencies::class);
        $agencies = $agenciesRepository->getBestAgenciesForMainPage();
        return $this->render('@appBundle/main/main.html.twig', ['title' => "Главная", "agencies" => $agencies]);

    }
}