<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Advert;
use AppBundle\Entity\Image;
use AppBundle\Form\ImageType;
use AppBundle\Entity\User;
use AppBundle\Form\AdvertType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\Collection;


class AddController extends Controller
{
    public function advertAction(Request $request)
    {
        $advert =  new Advert();
        $form = $this->createForm(AdvertType::class, $advert);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $advert->setUser($this->getUser());
            $em->persist($advert);
            /** @var File $file */;
            $headpiece = $form['image']['headpiece']->getData();
            foreach ($form['image']['files']->getData() as $image) {
                $file = $this->get('app.images_uploader')->upload($image, $advert, $headpiece);
                $em->persist($file);
            }
            $em->flush();
            return $this->redirectToRoute('app_homepage');
        }

        return $this->render("@appBundle/add/advert.html.twig", ["title" => "Добавление объявления", 'form' => $form->createView()]);
    }


}

