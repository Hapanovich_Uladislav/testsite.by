<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\RegistrationType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class RegistrationController.
 */
class RegistrationController extends Controller
{
    /**
     * Registration handler.
     *
     * @param Request $request
     * @param string $hash
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $errors = [];
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);
        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository(User::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $validator = $this->get('validator');
            $errors = $validator->validate($user);
            if (!$errors->count()) {
                $password = $this->get('security.password_encoder')
                    ->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
                $user->generateHash();
                $em->persist($user);
                $em->flush();
                $this->autoLoginUser($user);

                return $this->redirect($this->generateUrl('app_homepage'));
            }
        } else {
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        }

        return $this->render('@appBundle/Registration/registration.html.twig', array(
            'registrationForm' => $form->createView(),
            'errors' => $errors,
        ));
    }

    /**
     * Login user after successfully registration.
     *
     * @param $user
     */
    private function autoLoginUser($user)
    {
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }
}
