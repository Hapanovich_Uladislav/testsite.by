<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Agencies;

class AgenciesController extends Controller
{
    public function indexAction()
    {
        $agenciesRepository = $this->getDoctrine()->getRepository(Agencies::class);
        $agencies = $agenciesRepository->FindAllForMiniature();
        return $this->render('@appBundle/agencies/agencies.html.twig', ['title' => "Агенства", 'agencies' => $agencies]);
    }
}