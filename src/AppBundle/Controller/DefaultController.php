<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Task;
use AppBundle\Form\TaskType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{

    public function createAction()
    {
        $product = new Product();
        $product->setName('Шахматы');
        $product->setPrice(19.99);
        $product->setDescription('Эргономично и стильно!');

        $em = $this->getDoctrine()->getManager();

        // tells Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($product);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return $this->redirect($this->generateUrl('show', ["productId" => $product->getId()]));
    }



    public function showAction($productId=12)
    {
        $product = $this->getDoctrine()
            ->getRepository('AppBundle:Product')
            ->find($productId);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$productId
            );
        }

        return new Response('Saved new product with id '.$product->getName());

        // ... do something, like pass the $product object into a template
    }


    public function newAction(Request $request)
    {

        return $this->render('@appBundle/default/new.html.twig');
    }
}