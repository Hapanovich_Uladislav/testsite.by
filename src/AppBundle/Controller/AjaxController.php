<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Agencies;
use AppBundle\Entity\Advert;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class AjaxController extends Controller
{

    public function getAgencyByIdAction($id)
    {
        $agenciesRepository = $this->getDoctrine()->getRepository(Agencies::class);
        $agency = $agenciesRepository->findOneBy(['id' => $id]);

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $agency = $serializer->serialize($agency, 'json');

        return new JsonResponse($agency);
    }

    public function getMaxNumberOfObjectsAction()
    {
        $agenciesRepository = $this->getDoctrine()->getRepository(Agencies::class);
        $MaxNumberOfObjects = $agenciesRepository->getMaxAndMinNumber();

        return new JsonResponse($MaxNumberOfObjects);
    }

    public function getMaxAndMinPriceAction()
    {
        $advertRepository = $this->getDoctrine()->getRepository(Advert::class);
        $MaxAndMinPrice = $advertRepository->getMaxAndMinPriceQuery();

        return new JsonResponse($MaxAndMinPrice);
    }
}