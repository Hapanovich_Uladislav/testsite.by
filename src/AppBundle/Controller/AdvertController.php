<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Advert;

class AdvertController extends Controller
{

    public function indexAction()
    {
        $advertRepository = $this->getDoctrine()->getRepository(Advert::class);
        $adverts = $advertRepository->findAll();
        return $this->render('@appBundle/advert/advert.html.twig', ['title' => "Объявления", 'adverts' => $adverts]);
    }
}