<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Entity\Image;
use AppBundle\Entity\Advert;

class FileUploader
{
    private $targetDir;

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function upload(UploadedFile $file, Advert $advert, $headpiece)
    {
        if($file->getClientOriginalName() == $headpiece) {
            $headpiece = true;
        } else {
            $headpiece = false;
        }
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $image = new Image($advert, $fileName, $file->getSize(), $headpiece);
        $file->move($this->targetDir, $fileName);

        return $image;
    }
}